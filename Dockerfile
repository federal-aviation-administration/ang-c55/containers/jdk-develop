FROM almalinux:8.10-minimal-20240528

ARG JDK_VERSION

LABEL org.opencontainers.image.base.name="almalinux:8.10-minimal-20240528" \
      org.opencontainers.image.base.digest="sha256:496090f497df57c6e85f36eb491a878621fef8da5d1530d0f8fe53f09a3a90a0" \
      org.opencontainers.image.description="Docker container to support development of JDK based projects" \
      org.opencontainers.image.licenses="CC0-1.0" \ 
      org.opencontainers.image.vendor="US DOT, FAA, Office of NextGen" 

# Base layer(s)
RUN microdnf upgrade && \
    microdnf install --nodocs glibc-langpack-en && \
    microdnf clean all && \ 
    rm -rf /var/cache/yum

ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8

# SSH layer(s)
RUN microdnf upgrade && \
    microdnf install --nodocs openssh-clients sshpass && \
    microdnf clean all && \ 
    rm -rf /var/cache/yum

RUN mkdir -p /root/.ssh && \
    chmod 700 /root/.ssh

# JDK Layer(s)
ADD includes/adoptium.repo /etc/yum.repos.d/adoptium.repo

ENV JAVA_HOME=/usr/lib/jvm/temurin-${JDK_VERSION}-jdk/

# findutils includes xargs, which Gradle needs
RUN microdnf install --nodocs findutils temurin-${JDK_VERSION}-jdk && \
    microdnf clean all && \ 
    rm -rf /var/cache/yum && \
    rm -rf ${JAVA_HOME}/src.zip && \
    rm -rf ${JAVA_HOME}/man && \
    rm -rf ${JAVA_HOME}/sample
